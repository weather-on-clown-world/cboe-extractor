# CBOE Data Extractor
Takes a directory of zipped or unzipped CSV files of CBOE option chain data and saves the option contracts found within to a Postgres database.

```console
Usage: cboe-extractor <config-file-path>
```

Each row in the CSV files is saved to a table that is unique to the row's underlying symbol. The name of the table will be the lower case version of the underlying symbol with the "^" symbol removed from it if it exists. For example, if a row has an underlying symbol of "^SPX" then it will be stored in the table called "spx". The table will be created if it doesn't exist. It is recommended to that the tables either do not exist or are empty. This is because the program inserts rows ordered by the rows' `quote_datetime` field in order to improve the performance of inserting rows into a TimescaleDB database. If rows already exist in the tables then the DBMS has to reorganize the existing data to fit the new data which a TimescaleDB database is not optimized for. The tables will be created with the following statement:
```sql
CREATE TABLE $symbol (
    quote_datetime TIMESTAMPTZ NOT NULL,
    expiration DATE NOT NULL,
    strike BIGINT NOT NULL,
    open BIGINT NOT NULL,
    high BIGINT NOT NULL,
    low BIGINT NOT NULL,
    close BIGINT NOT NULL,
    trade_volume BIGINT NOT NULL,
    bid_size BIGINT NOT NULL,
    bid BIGINT NOT NULL,
    ask_size BIGINT NOT NULL,
    ask BIGINT NOT NULL,
    implied_volatility REAL NOT NULL,
    delta REAL NOT NULL,
    gamma REAL NOT NULL,
    theta REAL NOT NULL,
    vega REAL NOT NULL,
    rho REAL NOT NULL,
    open_interest BIGINT NOT NULL,
    is_call BOOLEAN NOT NULL,
    PRIMARY KEY(time, expiration, strike, is_call)
);
```
Any field whose unit is currency will be stored in the table as a "Microdollar". This is just the dollar amount represented as a floating point number multiplied by 1e6 and then truncated to an integer. This is because floating point number cannot represent base 10 numbers (like how we represent money in the real world) accurately. In order to get the actual dollar value from a column just divide the number by 1e6. For example, the value $123.45 will be stored as 123450000.

If the program fails for whatever reason. Any data already saved to the database will not be removed and will require manual intervention.
 {: .alert .alert-warning}

CBOE data may contian multiple option contracts whose quote datetime, expiration, strike, and option type are the same. In this situation only the first option contract found is saved and the other duplicates are not.
 {: .alert .alert-warning}

## Config File
The CBOE extractor requires a YAML configuration file in order to run. Here are the contents of the configuration file:
* `data_store` - Identifies the Postgres database that will store the CBOE option contracts.
  * `application_name <String>` - Sets the `application_name` parameter when connecting to the Postgres database. Default is an empty string.
  * `connect_timeout_seconds <Integer>` - The time limit in seconds applied to each socket-level connection attempt. Default is no timeout.
  * `dbname <String>` -  Name of the database to connect to. Defaults to value of `username`.
  * `host <String>` - Host to connect to
  * `keepalives_idle_seconds <Integer>`  - Number of seconds of inactivity after which a keepalive message is sent to the server. This option is ignored when connecting with Unix sockets. Defaults to 2 hours.
  * `password <String>` - Password to authenticate with.
  * `port <Integer>` - Port to connect to. Defaults to 5432.
  * `user <String>` - Username to authenticate with.
* `data_store_schema <String>` - The schema within the database to store the CBOE option contracts. Default is the current schema when connecting to the Postgres database (Which Postgres defaults to `public`).
* `cboe_data_dir <String>` - The directory that contains the CBOE csv files either zipped or unzipped
* `max_concurrent_files_processed <Integer>` - Maximum number of files to be parsed concurrently.