use arrayvec::ArrayString;
use chrono::{DateTime, NaiveDate, Utc};
use finance::{Microdollar, OptionsContract};
use futures::{channel::mpsc, Sink, SinkExt, Stream, StreamExt};
use serde::Deserialize;
use std::{
    cmp::Ordering,
    collections::HashMap,
    env,
    fmt::{self, Display, Formatter},
    io::Cursor,
    path::{Path, PathBuf},
};
use tokio::{
    fs,
    io::{self, AsyncWriteExt},
    task::{JoinError, JoinHandle},
};
use tokio_postgres::{binary_copy::BinaryCopyInWriter};
use tracing::{error, instrument, warn};
use tracing_subscriber::FmtSubscriber;
use zip::read::ZipArchive;

macro_rules! path_to_str {
    ($path: expr) => {
        $path.to_str().unwrap()
    };
}

macro_rules! path_to_string {
    ($path: expr) => {
        $path.to_str().unwrap().to_owned()
    };
}

#[tokio::main]
async fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("Usage: cboe-extractor <config-file-path>");
        return;
    }

    let subscriber = FmtSubscriber::builder().finish();
    if let Err(e) = tracing::subscriber::set_global_default(subscriber) {
        eprintln!("Failed to set global default subscriber: {}", e);
        return;
    }

    // Get configuration information
    let config_file_path = &args[1];
    let config_file = match std::fs::File::open(config_file_path) {
        Ok(file) => file,
        Err(e) => {
            error!("Failed to open file \"{}\": {}", config_file_path, e);
            return;
        }
    };
    let mut config: Config = match serde_yaml::from_reader(config_file) {
        Ok(config) => config,
        Err(e) => {
            error!("Failed to parse config: {}", e);
            return;
        }
    };
    config.data_store.require_write = Some(true);

    // Prepare database
    let (client, connection) = match config
        .data_store
        .create_client(tokio_postgres::tls::NoTls)
        .await
    {
        Ok(x) => x,
        Err(e) => {
            error!("Failed to create client for data store: {}", e);
            return;
        }
    };
    let schema = match config.data_store_schema {
        Some(ref schema) => schema.clone(),
        None => match config.data_store.get_current_schema().await {
            Ok(schema) => schema,
            Err(e) => {
                error!("Failed to get current schema: {}", e);
                return;
            }
        },
    };
    drop(client);
    drop(connection);
    if let Err(e) = process_cboe_dir(
        config.cboe_data_dir,
        config.data_store,
        schema,
        config.max_concurrent_files_processed,
    )
    .await
    {
        error!("Failed to process CBOE directory: {}", e);
    }
}

/// Handles the results of the tasks that process files.
async fn process_cboe_dir<P>(
    cboe_dir: P,
    data_store_config: PostgresConfig,
    schema: String,
    max_concurrent_files_processed: usize,
) -> Result<(), ProcessCboeDirError>
where
    P: AsRef<Path>,
{
    async fn get_cboe_file_paths<P>(dir: P) -> Result<Vec<PathBuf>, GetCboeFilePathsError>
    where
        P: AsRef<Path>,
    {
        let mut dir_rows = fs::read_dir(dir.as_ref())
            .await
            .map_err(|e| GetCboeFilePathsError::FailedToReadDirectory(e))?;
        let mut cboe_data_file_paths: Vec<(NaiveDate, PathBuf)> = Vec::new();
        while let Some(dir_row) = dir_rows
            .next_entry()
            .await
            .map_err(|e| GetCboeFilePathsError::FailedToGetDirectoryRow(e))?
        {
            let file_path = dir_row.path();
            let file_path_str = path_to_str!(file_path);
            // Will cause an issue if the file does not contain the date at the end of its name.
            let date_str = &file_path_str
                [file_path_str.len() - "yyyy-mm-dd.zip".len()..file_path_str.len() - ".zip".len()];
            let date = NaiveDate::parse_from_str(date_str, "%Y-%m-%d").map_err(|e| {
                GetCboeFilePathsError::FailedToGetDateFromFileName {
                    file_name: file_path_str.to_owned(),
                    reason: e,
                }
            })?;
            cboe_data_file_paths.push((date, file_path));
        }

        cboe_data_file_paths.sort_unstable_by(|(date, _), (other_date, _)| date.cmp(other_date));
        Ok(cboe_data_file_paths
            .into_iter()
            .map(|(_, file_path)| file_path)
            .collect())
    }

    fn get_table_name(underlying_symbol: &ArrayString<5>) -> ArrayString<5> {
        let mut table_name = underlying_symbol.clone();
        if let Some(c) = underlying_symbol.chars().next() {
            if c == '^' {
                table_name.remove(0);
            }
        }
        table_name.make_ascii_lowercase();
        table_name
    }

    async fn create_parse_tasks<I, S>(
        cboe_file_paths: I,
        mut parse_tasks: S,
    ) -> Result<(), CreateParseTasksError>
    where
        I: IntoIterator<Item = PathBuf>,
        S: Sink<
                JoinHandle<
                    Result<HashMap<ArrayString<5>, Vec<OptionsContract>>, ParseZippedCboeFileError>,
                >,
            > + Unpin,
    {
        for file_path in cboe_file_paths {
            parse_tasks
                .send(tokio::spawn(parse_zipped_cboe_file(file_path)))
                .await
                .map_err(|_| CreateParseTasksError::FailedToSendParseTask)?;
        }
        Ok(())
    }

    if max_concurrent_files_processed == 0 {
        return Err(ProcessCboeDirError::MaxConcurrentFilesProcessedIsZero);
    }

    let cboe_file_paths = get_cboe_file_paths(cboe_dir)
        .await
        .map_err(|e| ProcessCboeDirError::FailedToGetCboeFilePaths(e))?;
    let num_files = cboe_file_paths.len();
    let num_digits_in_num_files = ((num_files as f64).log10() as usize) + 1;
    let mut files_parsed = 0;
    let mut stdout = io::stdout();
    let (parse_tasks_wx, mut parse_tasks_rx) = mpsc::channel(32);
    let create_task = tokio::spawn(create_parse_tasks(cboe_file_paths, parse_tasks_wx));

    let mut send_tasks: HashMap<
        ArrayString<5>,
        (
            JoinHandle<Result<(), SendOptionsContractsToDataStoreError>>,
            mpsc::Sender<OptionsContract>,
        ),
    > = HashMap::new();
    while let Some(res) = parse_tasks_rx.next().await {
        let res = res
            .await
            .map_err(|e| ProcessCboeDirError::FailedToJoinParseTask(e))?;
        let options_contracts = res.map_err(|e| ProcessCboeDirError::ParseTaskFailed(e))?;
        files_parsed += 1;
        let msg = format!(
            "\rFiles parsed: {:0width$}/{:0width$}",
            files_parsed,
            num_files,
            width = num_digits_in_num_files
        );
        let _ = stdout.write_all(msg.as_bytes()).await;
        let _ = stdout.flush().await;

        for (underlying_symbol, options_contracts) in options_contracts.into_iter() {
            let table_name = get_table_name(&underlying_symbol);
            let options_contracts_wx =
                if let Some((_, options_contracts_wx)) = send_tasks.get_mut(&table_name) {
                    options_contracts_wx
                } else {
                    let (options_contracts_wx, options_contracts_rx) = mpsc::channel(65536);
                    let (data_store_client, connection) = data_store_config
                        .create_client(tokio_postgres::tls::NoTls)
                        .await
                        .map_err(|e| ProcessCboeDirError::FailedToGetDataStoreClient(e))?;
                    tokio::spawn(async move {
                        if let Err(e) = connection.await {
                            error!("Error occurred in Postgres connection: {}", e);
                        }
                    });
                    let send_task = tokio::spawn(send_options_contracts_to_data_store(
                        data_store_client,
                        options_contracts_rx,
                        table_name,
                        schema.clone(),
                    ));
                    send_tasks.insert(table_name, (send_task, options_contracts_wx));
                    let (_, options_contracts_wx) = send_tasks.get_mut(&table_name).unwrap();
                    options_contracts_wx
                };
            for options_contract in options_contracts.into_iter() {
                if let Err(_) = options_contracts_wx.send(options_contract).await {
                    let (send_task, _) = send_tasks.remove(&table_name).unwrap();
                    let res = send_task
                        .await
                        .map_err(|e| ProcessCboeDirError::FailedToJoinSendTask(e))?;
                    res.map_err(|e| ProcessCboeDirError::SendTaskFailed(e))?;
                    return Err(ProcessCboeDirError::FailedToSendOptionsContractsToSendTask);
                }
            }
        }
    }
    let send_tasks = send_tasks.into_values().map(|(task, _)| task);
    let send_tasks_res = futures::future::join_all(send_tasks).await;
    for res in send_tasks_res.into_iter() {
        let res = res.map_err(|e| ProcessCboeDirError::FailedToJoinSendTask(e))?;
        res.map_err(|e| ProcessCboeDirError::SendTaskFailed(e))?;
    }
    let create_task_res = create_task
        .await
        .map_err(|e| ProcessCboeDirError::FailedToJoinCreateTask(e))?;
    create_task_res.map_err(|e| ProcessCboeDirError::CreateTaskFailed(e))?;
    let _ = stdout.write_all(b"\nFinished\n");

    Ok(())
}

async fn send_options_contracts_to_data_store<S>(
    data_store_client: tokio_postgres::Client,
    mut options_contracts: S,
    table_name: ArrayString<5>,
    schema: String,
) -> Result<(), SendOptionsContractsToDataStoreError>
where
    S: Stream<Item = OptionsContract> + Unpin,
{
    let copy_query = finance::get_options_contract_table_copy_query(&table_name, &schema);

    postgres_utils::create_postgres_table(
        &table_name,
        &schema,
        &data_store_client,
        finance::OPTIONS_CONTRACT_CREATE_TABLE_STATMENT_BODY,
    )
    .await
    .map_err(
        |e| SendOptionsContractsToDataStoreError::FailedToCreateTable {
            table_name: table_name.to_string(),
            reason: e,
        },
    )?;
    let copy_statement = data_store_client
        .prepare(&copy_query)
        .await
        .map_err(|e| SendOptionsContractsToDataStoreError::FailedToPrepareCopyStatement(e))?;
    let sink = data_store_client
        .copy_in(&copy_statement)
        .await
        .map_err(|e| SendOptionsContractsToDataStoreError::FailedToExecuteCopyStatement(e))?;
    let writer = BinaryCopyInWriter::new(sink, &finance::OPTIONS_CONTRACT_TABLE_COPY_QUERY_TYPES);
    futures::pin_mut!(writer);
    while let Some(options_contract) = options_contracts.next().await {
        writer
            .as_mut()
            .write(&[
                &options_contract.quote_datetime,
                &options_contract.expiration,
                &options_contract.strike,
                &options_contract.open,
                &options_contract.high,
                &options_contract.low,
                &options_contract.close,
                &(options_contract.trade_volume as i64),
                &(options_contract.bid_size as i64),
                &options_contract.bid,
                &(options_contract.ask_size as i64),
                &options_contract.ask,
                &options_contract.implied_volatility,
                &options_contract.delta,
                &options_contract.gamma,
                &options_contract.theta,
                &options_contract.vega,
                &options_contract.rho,
                &(options_contract.open_interest as i64),
                &options_contract.is_call,
            ])
            .await
            .map_err(|e| SendOptionsContractsToDataStoreError::FailedToWriteOptionsContract(e))?;
    }
    writer
        .finish()
        .await
        .map_err(|e| SendOptionsContractsToDataStoreError::FailedToFinishWriting(e))?;

    Ok(())
}

/// Parses a zipped CBOE csv file
#[instrument(skip(file_path))]
async fn parse_zipped_cboe_file<P>(
    file_path: P,
) -> Result<HashMap<ArrayString<5>, Vec<OptionsContract>>, ParseZippedCboeFileError>
where
    P: AsRef<Path>,
{
    let file_path = file_path.as_ref();
    let mut file = fs::File::open(file_path).await.map_err(|e| {
        ParseZippedCboeFileError::FailedToOpenFile {
            file_name: path_to_string!(file_path),
            reason: e,
        }
    })?;
    let mut file_contents = Vec::new();
    tokio::io::AsyncReadExt::read_to_end(&mut file, &mut file_contents)
        .await
        .map_err(|e| ParseZippedCboeFileError::FailedToReadFile {
            file_name: path_to_string!(file_path),
            reason: e,
        })?;
    let mut file_contents = Cursor::new(file_contents);
    let mut archive = ZipArchive::new(&mut file_contents).map_err(|e| {
        ParseZippedCboeFileError::FailedToReadZipArchive {
            file_name: path_to_string!(file_path),
            reason: e,
        }
    })?;
    if archive.len() == 0 {
        return Err(ParseZippedCboeFileError::ZipArchiveEmpty {
            file_name: path_to_string!(file_path),
        });
    }
    let zip_file =
        archive
            .by_index(0)
            .map_err(|e| ParseZippedCboeFileError::FailedToGetZipFile {
                file_name: path_to_string!(file_path),
                reason: e,
            })?;
    let mut reader = csv::ReaderBuilder::new()
        .has_headers(true)
        .terminator(csv::Terminator::CRLF)
        .from_reader(zip_file);
    let mut options_contracts: HashMap<ArrayString<5>, Vec<OptionsContract>> = HashMap::new();
    for row in reader.deserialize() {
        let row = match row {
            Ok(row) => row,
            Err(e) => {
                warn!(
                    "Failed to parse options contract from \"{}\", skipping row: {}",
                    path_to_str!(file_path),
                    e
                );
                continue;
            }
        };
        let row: CBOEDataRow = row;
        let underlying_symbol = row.underlying_symbol.clone();
        let options_contract: OptionsContract = row.into();
        if let Some(options_contracts) = options_contracts.get_mut(&underlying_symbol) {
            options_contracts.push(options_contract);
        } else {
            let key = underlying_symbol.clone();
            let value = vec![options_contract];
            options_contracts.insert(key, value);
        }
    }
    for options_contracts in options_contracts.values_mut() {
        options_contracts.sort();
        options_contracts.dedup();
    }
    Ok(options_contracts)
}

// +----------------------------------------------------------------------------------------------+
// | Structs                                                                                      |
// +----------------------------------------------------------------------------------------------+

#[derive(Deserialize)]
struct Config {
    /// Information about the TimescaleDB database to store the CBOE data.
    data_store: PostgresConfig,

    // The name of the schema where the tables that contain the CBOE data will be located.
    data_store_schema: Option<String>,

    /// Directory containing files of CBOE option chain data. These files are csv files or zip files of csv files.
    cboe_data_dir: PathBuf,

    /// Maximum number of files to be parsed concurrently.
    max_concurrent_files_processed: usize,
}

#[derive(Copy, Clone, Deserialize, PartialOrd, Ord, PartialEq, Eq)]
enum CBOEDataRowOptionType {
    #[serde(rename = "C")]
    Call,
    #[serde(rename = "P")]
    Put,
}

mod no_fractional_seconds_or_t {
    use chrono::{DateTime, TimeZone, Utc};
    use serde::{self, Deserialize, Deserializer};

    // This is the format of date times in CBOE data files.
    const FORMAT: &'static str = "%Y-%m-%d %H:%M:%S";

    pub fn deserialize<'de, D>(deserializer: D) -> Result<DateTime<Utc>, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        let datetime = chrono_tz::America::New_York
            .datetime_from_str(&s, FORMAT)
            .map_err(serde::de::Error::custom)?;

        Ok(datetime.with_timezone(&Utc))
    }
}

#[derive(Deserialize)]
struct CBOEDataRow {
    underlying_symbol: ArrayString<5>,
    #[serde(with = "no_fractional_seconds_or_t")]
    quote_datetime: DateTime<Utc>,
    #[serde(rename = "root")]
    _root: ArrayString<5>,
    expiration: NaiveDate,
    strike: Microdollar,
    option_type: CBOEDataRowOptionType,
    open: Microdollar,
    high: Microdollar,
    low: Microdollar,
    close: Microdollar,
    trade_volume: u64,
    bid_size: u64,
    bid: Microdollar,
    ask_size: u64,
    ask: Microdollar,
    #[serde(rename = "underlying_bid")]
    _underlying_bid: Microdollar,
    #[serde(rename = "underlying_ask")]
    _underlying_ask: Microdollar,
    #[serde(rename = "implied_underlying_price")]
    _implied_underlying_price: Microdollar,
    #[serde(rename = "active_underlying_price")]
    _active_underlying_price: Microdollar,
    implied_volatility: f32,
    delta: f32,
    gamma: f32,
    theta: f32,
    vega: f32,
    rho: f32,
    open_interest: u64,
}

impl Into<OptionsContract> for CBOEDataRow {
    fn into(self) -> OptionsContract {
        OptionsContract {
            quote_datetime: self.quote_datetime,
            expiration: self.expiration,
            strike: self.strike,
            is_call: match self.option_type {
                CBOEDataRowOptionType::Call => true,
                CBOEDataRowOptionType::Put => false,
            },
            open: self.open,
            high: self.high,
            low: self.low,
            close: self.close,
            trade_volume: self.trade_volume,
            bid_size: self.bid_size,
            bid: self.bid,
            ask_size: self.ask_size,
            ask: self.ask,
            implied_volatility: self.implied_volatility,
            delta: self.delta,
            gamma: self.gamma,
            theta: self.theta,
            vega: self.vega,
            rho: self.rho,
            open_interest: self.open_interest,
        }
    }
}

impl Ord for CBOEDataRow {
    fn cmp(&self, other: &Self) -> Ordering {
        // `underlying_symbol` not included because whenever a collection of `CBOEDataRow` gets sorted in this program they all have the same underlying symbol
        (
            self.quote_datetime,
            self.strike,
            self.expiration,
            self.option_type,
        )
            .cmp(&(
                other.quote_datetime,
                other.strike,
                other.expiration,
                other.option_type,
            ))
    }
}

impl PartialOrd for CBOEDataRow {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for CBOEDataRow {
    fn eq(&self, other: &Self) -> bool {
        (
            &self.underlying_symbol,
            self.quote_datetime,
            self.strike,
            self.expiration,
            self.option_type,
        ) == (
            &other.underlying_symbol,
            other.quote_datetime,
            other.strike,
            other.expiration,
            other.option_type,
        )
    }
}

impl Eq for CBOEDataRow {}

// +----------------------------------------------------------------------------------------------+
// | Errors                                                                                       |
// +----------------------------------------------------------------------------------------------+

#[derive(Debug)]
enum ProcessCboeDirError {
    FailedToJoinParseTask(JoinError),
    ParseTaskFailed(ParseZippedCboeFileError),
    FailedToGetCboeFilePaths(GetCboeFilePathsError),
    FailedToGetDataStoreClient(tokio_postgres::Error),
    FailedToJoinCreateTask(JoinError),
    CreateTaskFailed(CreateParseTasksError),
    FailedToJoinSendTask(JoinError),
    SendTaskFailed(SendOptionsContractsToDataStoreError),
    FailedToSendOptionsContractsToSendTask,
    MaxConcurrentFilesProcessedIsZero,
}

impl std::error::Error for ProcessCboeDirError {}

impl Display for ProcessCboeDirError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Self::FailedToJoinParseTask(ref e) => {
                write!(f, "Failed to a join CBOE file parsing task: {}", e)
            }
            Self::ParseTaskFailed(ref e) => {
                write!(f, "A CBOE file parsing task has failed: {}", e)
            }
            Self::FailedToGetCboeFilePaths(ref e) => {
                write!(f, "Failed to get CBOE file paths: {}", e)
            }
            Self::FailedToGetDataStoreClient(ref e) => {
                write!(f, "Failed to get data store client: {}", e)
            }
            Self::FailedToJoinCreateTask(ref e) => {
                write!(f, "Failed to join task to create parse tasks: {}", e)
            }
            Self::CreateTaskFailed(ref e) => {
                write!(f, "Task to create parse tasks has failed: {}", e)
            }
            Self::FailedToJoinSendTask(ref e) => {
                write!(f, "Failed to join a send task: {}", e)
            }
            Self::SendTaskFailed(ref e) => {
                write!(
                    f,
                    "A task to send options contracts to the data store has failed: {}",
                    e
                )
            }
            Self::FailedToSendOptionsContractsToSendTask => {
                write!(f, "Failed to send options contracts to a send task")
            }
            Self::MaxConcurrentFilesProcessedIsZero => {
                write!(
                    f,
                    "Maximum number of files that can be processed concurrently is zero"
                )
            }
        }
    }
}

#[derive(Debug)]
enum ParseZippedCboeFileError {
    FailedToOpenFile {
        file_name: String,
        reason: std::io::Error,
    },
    FailedToReadFile {
        file_name: String,
        reason: std::io::Error,
    },
    FailedToGetZipFile {
        file_name: String,
        reason: zip::result::ZipError,
    },
    FailedToReadZipArchive {
        file_name: String,
        reason: zip::result::ZipError,
    },
    ZipArchiveEmpty {
        file_name: String,
    },
}

impl std::error::Error for ParseZippedCboeFileError {}

impl Display for ParseZippedCboeFileError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Self::FailedToOpenFile { file_name, reason } => {
                write!(f, "Failed to open file \"{}\": {}", file_name, reason)
            }
            Self::FailedToReadFile { file_name, reason } => {
                write!(f, "Failed to read file \"{}\": {}", file_name, reason)
            }
            Self::FailedToGetZipFile { file_name, reason } => {
                write!(
                    f,
                    "Failed to get zip file within archive \"{}\": {}",
                    file_name, reason
                )
            }
            Self::FailedToReadZipArchive { file_name, reason } => {
                write!(
                    f,
                    "Failed to read zip archive \"{}\": {}",
                    file_name, reason
                )
            }
            Self::ZipArchiveEmpty { file_name } => {
                write!(f, "Zip archive \"{}\" is empty", file_name)
            }
        }
    }
}

#[derive(Debug)]
enum GetCboeFilePathsError {
    FailedToReadDirectory(io::Error),
    FailedToGetDirectoryRow(io::Error),
    FailedToGetDateFromFileName {
        file_name: String,
        reason: chrono::ParseError,
    },
}

impl std::error::Error for GetCboeFilePathsError {}

impl Display for GetCboeFilePathsError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Self::FailedToReadDirectory(ref e) => {
                write!(f, "Failed to read directory: {}", e)
            }
            Self::FailedToGetDirectoryRow(ref e) => {
                write!(f, "Failed to get directory row: {}", e)
            }
            Self::FailedToGetDateFromFileName { file_name, reason } => {
                write!(
                    f,
                    "Failed to get date from name of CBOE file \"{}\": {}",
                    file_name, reason
                )
            }
        }
    }
}

#[derive(Debug)]
enum SendOptionsContractsToDataStoreError {
    FailedToCreateTable {
        table_name: String,
        reason: postgres_utils::CreatePostgresTableError,
    },
    FailedToPrepareCopyStatement(tokio_postgres::Error),
    FailedToExecuteCopyStatement(tokio_postgres::Error),
    FailedToWriteOptionsContract(tokio_postgres::Error),
    FailedToFinishWriting(tokio_postgres::Error),
}

impl std::error::Error for SendOptionsContractsToDataStoreError {}

impl Display for SendOptionsContractsToDataStoreError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Self::FailedToCreateTable { table_name, reason } => {
                write!(
                    f,
                    "Failed to create Postgres table \"{}\": {}",
                    table_name, reason
                )
            }
            Self::FailedToPrepareCopyStatement(ref e) => {
                write!(f, "Failed to prepare copy statement: {}", e)
            }
            Self::FailedToExecuteCopyStatement(ref e) => {
                write!(f, "Failed to execut copy statement: {}", e)
            }
            Self::FailedToWriteOptionsContract(ref e) => {
                write!(f, "Failed to write copy statement: {}", e)
            }
            Self::FailedToFinishWriting(ref e) => write!(f, "Failed to finish writing: {}", e),
        }
    }
}

#[derive(Debug)]
enum CreateParseTasksError {
    FailedToSendParseTask,
}

impl std::error::Error for CreateParseTasksError {}

impl Display for CreateParseTasksError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Self::FailedToSendParseTask => {
                write!(f, "Failed to send parse task")
            }
        }
    }
}
